const express = require('express');
const multer  = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const db = require('../mySqlDb');

const router = express.Router();
const storage = multer.diskStorage({
  destination: (req, file, cd) => {
    cd(null, config.uploadPath);
  },
  filename: (req, file, cd) => {
    cd(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
  try{
    const [objects] = await db.getConnection().execute('SELECT * FROM news');
    const objectsForUser = objects.map( obj => {
      return {
        id: obj.id,
        title: obj.title,
        image: obj.image,
        date: obj.date
      }
    })
    res.send(objectsForUser);
  } catch (e) {
    next(e);
  }
});


router.get('/:id', async (req, res, next) => {
  try{
    const [objects] = await db.getConnection().execute('SELECT * FROM news WHERE id = ?', [req.params.id]);
    const object = objects[0];
    if (!object){
      res.status(500).send('Not found!');
    } else {
      res.send(object);
    }
  } catch (e) {
    res.status(500).send('Please fill in the fields!');
    next(e);
  }
});

router.post('/',  upload.single('image'), async (req, res, next) => {
  try {
    if (!req.body.title || !req.body.description) {
      return res.status(500).send('Please fill in the fields!');
    }

    const object = {
      title: req.body.title,
      description: null,
      image: null,
      date: new Date()
    }

    if (req.body.description) {
      object.description = req.body.description;
    }

    if (req.file) {
      object.image = req.file.filename;
    }

    let query = 'INSERT INTO news (title, description, image, date) VALUES (?, ?, ?, ?)';

    const [result] = await db.getConnection().execute(query, [
      object.title,
      object.description,
      object.image,
      object.date
    ]);

    const postObject = {
      id: result.insertId,
      title: object.title,
      description: object.description,
      image:  object.image,
      date:  object.date
    }
    return res.send(postObject);
  } catch (e) {
    next(e);
  }
});



router.delete('/:id', async (req, res, next) => {
  try{
    const [result] = await db.getConnection().execute('DELETE FROM news WHERE id = ?', [req.params.id]);
    return res.send(result);
  } catch (e) {
    next(e);
  }
});

module.exports = router;