const express = require('express');
const db = require('../mySqlDb');

const router = express.Router();

router.get('/', async (req, res, next) => {
  if (req.query.news_id){
      try{
      const [objects] = await db.getConnection().execute('SELECT * FROM comment WHERE news_id = ?', [req.query.news_id]);
        return res.send(objects);
      } catch (e) {
        next(e);
      }
    } else {
      return res.status(500).send('Wrong news_id !');
    }
});

router.post('/',async (req, res, next) => {
  try {
    if (!req.body.news_id || !req.body.comment) {
      return res.status(500).send('Please fill in the fields!');
    }

    const object = {
      news_id: req.body.news_id,
      author: 'Anonymous',
      comment: req.body.comment
    }

    if (req.body.author){
      object.author = req.body.author
    }

    let query = 'INSERT INTO comment (news_id, author, comment) VALUES (?, ?, ?)';

    const [result] = await db.getConnection().execute(query, [
      object.news_id,
      object.author,
      object.comment
    ]);

    const postObject = {
      id: result.insertId,
      news_id:  object.news_id,
      author:  object.author,
      comment: object.comment
    }
    return res.send(postObject);
  } catch (e) {
    next(e);
  }
});

router.delete('/:id', async (req, res, next) => {
  try{
    const [result] = await db.getConnection().execute('DELETE FROM comment WHERE id = ?', [req.params.id]);
    return res.send(result);
  } catch (e) {
    next(e);
  }
});

module.exports = router;