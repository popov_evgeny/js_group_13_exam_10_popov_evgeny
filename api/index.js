const express = require('express');
const db = require('./mySqlDb');
const app = express();
const news = require('./app/news');
const cors = require('cors');
const comment = require('./app/comment');

const port = 8000;
app.use(cors({origin: 'http://localhost:4200'}));

app.use(express.json());
app.use(express.static('public'));
app.use('/news', news);
app.use('/comment', comment);

const run = async () => {
  await db.init();

  app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
  });
}

run().catch(e => {
  console.error(e);
});
