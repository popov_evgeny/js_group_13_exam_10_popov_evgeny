create schema popov collate utf8_general_ci;

use popov;

create table news
(
    id          int auto_increment
        primary key,
    title       varchar(255) not null,
    description text         not null,
    image       varchar(31)  null,
    date        timestamp    null
);

create table comment
(
    id      int auto_increment
        primary key,
    news_id int          not null,
    author  varchar(255) null,
    comment text         not null,
    constraint comment_news_id_fk
        foreign key (news_id) references news (id)
            on delete cascade
);

insert into news (id, title, description, image, date)
values  (11, 'asdasd', 'sadasd', '2BCosbkkiugsJEuppJ7Xl.jpg', '2022-02-12 18:02:38'),
        (19, 'asdasd', 'asdasd', 'MshySkcU3I2g5Z-4BC5Bh.jpg', '2022-02-12 20:11:48');

insert into comment (id, news_id, author, comment)
values  (47, 11, 'asdasd', 'asdasdas'),
        (48, 19, 'adsasdasd', 'asdasdas'),
        (49, 19, 'adasda', 'asdasdas');