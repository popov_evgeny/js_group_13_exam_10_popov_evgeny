export class CommentModel {
  constructor(
    public id: number,
    public news_id: number,
    public author: string,
    public comment: string
  ) {}
}

export interface CommentData {
  news_id: number,
  author: string,
  comment: string
}
