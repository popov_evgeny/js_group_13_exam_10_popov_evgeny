export class News {
  constructor(
    public id: number,
    public title: string,
    public description: string,
    public image: string,
    public date: string
  ) {}
}

export interface NewsData {
  title: string,
  description: string,
  image: string
}
