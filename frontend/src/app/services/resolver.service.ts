import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { EMPTY, Observable, of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { NewsService } from './news.service';
import { News } from '../models/news.model';

@Injectable({
  providedIn: 'root'
})
export class ResolverService implements Resolve<News> {

  constructor(
    private newsService: NewsService,
    private router: Router
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<News> | Observable<never>{
    const id = <string>route.params['id'];
    return this.newsService.getNewsData(id).pipe(mergeMap(newsData => {
      if (newsData) {
        return of (newsData);
      }
      void this.router.navigate(['/']);
      return EMPTY;
    }));
  }
}
