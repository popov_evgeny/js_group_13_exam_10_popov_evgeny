import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { News, NewsData } from '../models/news.model';
import { map } from 'rxjs/operators';
import { CommentData, CommentModel } from '../models/comment.model';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NewsService {
  private arrNews: News[] = [];
  private arrComments: CommentModel[] = [];
  changeArrayNews = new Subject<News[]>();
  changeArrayComments = new Subject<CommentModel[]>();
  newsLoading = new Subject<boolean>();
  commentLoading = new Subject<boolean>();

  constructor(private http: HttpClient) { }

  getNews() {
    this.newsLoading.next(true);
    this.http.get<News[]>(environment.apiUrl + '/news').pipe(
      map(response => {
        return response.map(newsData => {
          return new News(
            newsData.id,
            newsData.title,
            newsData.description,
            newsData.image,
            newsData.date
          );
        });
      })
    ).subscribe( {
      next: news => {
        this.arrNews = news;
        this.changeArrayNews.next(this.arrNews.slice());
        this.newsLoading.next(false);
      },
      error: error => {
        console.error({message: error});
        this.newsLoading.next(false);
      }
    });
  }

  getComments(id: number) {
    this.commentLoading.next(true);
    this.http.get<CommentModel[]>(environment.apiUrl + '/comment?news_id=' + id).pipe(
      map(response => {
        return response.map( commentData => {
          return new CommentModel(
            commentData.id,
            commentData.news_id,
            commentData.author,
            commentData.comment
          );
        });
      })
    ).subscribe( {
      next: comments => {
        this.arrComments = comments;
        this.changeArrayComments.next(this.arrComments.slice());
        this.commentLoading.next(false);
      },
      error: error => {
        console.error(error);
        this.commentLoading.next(false);
      }
    });
  }

  getNewsData(id: string){
    return this.http.get<News | null>(environment.apiUrl + '/news/' + id ).pipe(map(result => {
      if (!result) return null;
      return result;
    }));
  }

  createNews(newsData: NewsData) {
    const formData = new FormData();
    formData.append('title', newsData.title);
    formData.append('description', newsData.description);
    if (newsData.image) {
      formData.append('image', newsData.image);
    }
    return this.http.post(environment.apiUrl + '/news', formData);
  }

  createComment(comment: CommentData) {
    return this.http.post(environment.apiUrl + '/comment', comment);
  }

  removeNews(id: number) {
    this.http.delete(environment.apiUrl + '/news/' + id).subscribe( {
      next: () => {
        this.getNews();
      },
      error: (e) => {
        console.error(e)
      }
    });
  }

  removeComment(id: number) {
    return this.http.delete(environment.apiUrl + '/comment/' + id);
  }

}
