import { Component, OnDestroy, OnInit } from '@angular/core';
import { News } from '../../models/news.model';
import { NewsService } from '../../services/news.service';
import { environment } from '../../../environments/environment';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.sass']
})
export class NewsComponent implements OnInit, OnDestroy {
  arrayNews: News[] = [];
  pathImg = environment.apiUrl;
  loading = false;
  arrSubscription!: Subscription;
  loadingSubscription!: Subscription;

  constructor(private newsService: NewsService) { }

  ngOnInit(): void {
    this.loadingSubscription = this.newsService.newsLoading.subscribe( loading => {
      this.loading = loading;
    });
    this.arrSubscription = this.newsService.changeArrayNews.subscribe( arr => {
      this.arrayNews = arr;
    });
    this.newsService.getNews();
  }

  onRemoveNews(id: number) {
    this.newsService.removeNews(id);
  }

  ngOnDestroy() {
    this.loadingSubscription.unsubscribe();
    this.arrSubscription.unsubscribe();
  }
}
