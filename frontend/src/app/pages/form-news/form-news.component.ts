import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NewsService } from '../../services/news.service';
import { Router } from '@angular/router';
import { NewsData } from '../../models/news.model';

@Component({
  selector: 'app-form-news',
  templateUrl: './form-news.component.html',
  styleUrls: ['./form-news.component.sass']
})
export class FormNewsComponent implements OnInit {
  @ViewChild('form') form!: NgForm;

  constructor(
    private newsService: NewsService,
    private router: Router,
  ) { }

  ngOnInit(): void {
  }

  onSubmit() {
    const newsData: NewsData = this.form.value;
    this.newsService.createNews(newsData).subscribe(() => {
      void this.router.navigate(['/']);
    });
    this.form.resetForm();
  }
}
