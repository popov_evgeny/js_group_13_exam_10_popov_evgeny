import { Component, OnInit } from '@angular/core';
import { News } from '../../models/news.model';
import { ActivatedRoute } from '@angular/router';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-news-info',
  templateUrl: './news-info.component.html',
  styleUrls: ['./news-info.component.sass']
})
export class NewsInfoComponent implements OnInit {
  news: News | null = null;
  pathImg = environment.apiUrl;

  constructor(private route: ActivatedRoute,) { }

  ngOnInit(): void {
    this.route.data.subscribe(data => {
        this.news = <News | null >data['news'];
    });

  }

}
