import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NewsService } from '../../services/news.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CommentData } from '../../models/comment.model';
import { News } from '../../models/news.model';

@Component({
  selector: 'app-form-comment',
  templateUrl: './form-comment.component.html',
  styleUrls: ['./form-comment.component.sass']
})
export class FormCommentComponent implements OnInit {
  @ViewChild('form') form!: NgForm;
  news: News | null = null;
  newsId!: number;

  constructor(
    private newsService: NewsService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.route.data.subscribe(data => {
      this.news = <News | null >data['news'];
    });
    if (this.news?.id){
      this.newsId = this.news.id;
    }
  }

  onSubmit() {
    const commentData: CommentData = {news_id: this.newsId, author: this.form.value.author, comment: this.form.value.comment};
    this.newsService.createComment(commentData).subscribe(() => {
      this.newsService.getComments(this.newsId);
    });
    this.form.resetForm();
  }
}
