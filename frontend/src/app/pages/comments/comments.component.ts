import { Component, OnDestroy, OnInit } from '@angular/core';
import { News } from '../../models/news.model';
import { NewsService } from '../../services/news.service';
import { ActivatedRoute } from '@angular/router';
import { CommentModel } from '../../models/comment.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.sass']
})
export class CommentsComponent implements OnInit, OnDestroy{
  arrayComments: CommentModel[] | null = null;
  news: News | null = null;
  newsId!: number;
  loading = false;
  arrSubscription!: Subscription;
  loadingSubscription!: Subscription;

  constructor(
    private newsService: NewsService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.loadingSubscription = this.newsService.commentLoading.subscribe( loading => {
      this.loading = loading;
    });
    this.arrSubscription = this.newsService.changeArrayComments.subscribe( arr => {
      this.arrayComments = arr;
    });
    this.route.data.subscribe(data => {
      this.news = <News | null >data['news'];
    });
    if (this.news?.id){
      this.newsId = this.news.id;
    }
    this.newsService.getComments(this.newsId);
  }

  onRemoveComment(id: number) {
    this.newsService.removeComment(id).subscribe({
      next: () => {
        this.newsService.getComments(this.newsId);
      },
      error: (e) => {
        console.error(e);
      }
  });
  }

  ngOnDestroy() {
    this.loadingSubscription.unsubscribe();
    this.arrSubscription.unsubscribe();
  }
}
