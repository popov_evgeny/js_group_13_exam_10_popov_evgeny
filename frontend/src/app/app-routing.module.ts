import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewsComponent } from './pages/news/news.component';
import { FormNewsComponent } from './pages/form-news/form-news.component';
import { ResolverService } from './services/resolver.service';
import { NewsInfoComponent } from './pages/news-info/news-info.component';

const routes: Routes = [
  { path: '', component: NewsComponent},
  { path: 'news/creat-news', component: FormNewsComponent},
  { path: 'news/:id', component: NewsInfoComponent, resolve: {news: ResolverService}},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
